#pipeline.py


from kedro.pipeline import Pipeline, node
from .nodes import load_model, predict

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=load_model,
                inputs="params:weights_path",
                outputs="yolov5_loaded_model",
                name="load_yolov5_model"
            ),
            node(
                func=predict,
                inputs=dict(
                    model="yolov5_loaded_model",
                    image_path="params:image_path"
                ),
                outputs="prediction_results",
                name="predict_with_yolov5"
            ),
        ]
    )