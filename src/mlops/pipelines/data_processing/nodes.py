#nodes.py

from yolov5 import detect

def load_model(weights_path):
    # Load the YOLOv5 model
    model = detect.DetectMultiBackend(weights=weights_path)
    return model

def predict(model, image_path):
    # Perform prediction using YOLOv5
    results = model.predict(source=image_path)
    return results