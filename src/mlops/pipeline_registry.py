# pipeline_registry.py
from kedro.framework.project import find_pipelines
#import mlops.pipelines.data_engineering as pipeline_engineering
#import mlops.pipelines.data_processing  as pipeline_processing

def register_pipelines():
    #dp = pipeline_processing.create_pipeline()
    #de= pipeline_engineering.create_pipeline()
    pipelines = find_pipelines()
    print(sum(pipelines.values()))
    return {
       
        "__default__": sum(pipelines.values()),
    }